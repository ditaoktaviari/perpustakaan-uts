<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\Dashboard\DashboardController::class,'index'])->name('dashboard');


Route::get('/tampilMahasiswa', [App\Http\Controllers\Mahasiswa\MahasiswaController::class,'tampilMahasiswa'])->name('tampilMahasiswa');

Route::get('/createMahasiswa', [App\Http\Controllers\Mahasiswa\MahasiswaController::class,'create'])->name('createMahasiswa');
Route::post('/insertMahasiswa', [App\Http\Controllers\Mahasiswa\MahasiswaController::class,'insert'])->name('insertMahasiswa');

Route::get('/editMahasiswa/{id}', [App\Http\Controllers\Mahasiswa\MahasiswaController::class,'edit'])->name('editMahasiswa');
Route::post('/updateMahasiswa', [App\Http\Controllers\Mahasiswa\MahasiswaController::class,'update'])->name('updateMahasiswa');

Route::get('/deleteMahasiswa/{id}', [App\Http\Controllers\Mahasiswa\MahasiswaController::class,'delete'])->name('deleteMahasiswa');


Route::get('/tampilBuku', [App\Http\Controllers\Buku\BukuController::class,'tampilBuku'])->name('tampilBuku');

Route::get('/createBuku', [App\Http\Controllers\Buku\BukuController::class,'create'])->name('createBuku');
Route::post('/insertBuku', [App\Http\Controllers\Buku\BukuController::class,'insert'])->name('insertBuku');

Route::get('/editBuku/{id}', [App\Http\Controllers\Buku\BukuController::class,'edit'])->name('editBuku');
Route::post('/updateBuku', [App\Http\Controllers\Buku\BukuController::class,'update'])->name('updateBuku');

Route::get('/deleteBuku/{id}', [App\Http\Controllers\Buku\BukuController::class,'delete'])->name('deleteBuku');


Route::get('/tampilTransaksi', [App\Http\Controllers\Transaksi\TransaksiController::class,'tampilTransaksi'])->name('tampilTransaksi');

Route::get('/createTransaksi', [App\Http\Controllers\Transaksi\TransaksiController::class,'create'])->name('createTransaksi');
Route::post('/insertTransaksi', [App\Http\Controllers\Transaksi\TransaksiController::class,'insert'])->name('insertTransaksi');

Route::get('/editTransaksi/{id}', [App\Http\Controllers\Transaksi\TransaksiController::class,'edit'])->name('editTransaksi');
Route::post('/updateTransaksi', [App\Http\Controllers\Transaksi\TransaksiController::class,'update'])->name('updateTransaksi');

Route::get('/deleteTransaksi/{id}', [App\Http\Controllers\Transaksi\TransaksiController::class,'delete'])->name('deleteTransaksi');


