<?php

namespace App\Http\Controllers\Mahasiswa;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB; 

class MahasiswaController extends Controller
{
    public function tampilMahasiswa(){
        $mhs = DB::table('mahasiswa')->get();
        return view('mahasiswa.mahasiswaList',['data_mhs'=>$mhs, 'page'=>'mhs', ]);
    }

    public function create(){
        return view('mahasiswa.mahasiswaCreate');
    }

    public function insert(Request $request){
        DB::table('mahasiswa')->insert([
            'nama'=>$request->nama,
            'nim'=>$request->nim,
            'email'=>$request->email,
            'no_telp'=>$request->no_telp,
            'prodi'=>$request->prodi,
            'jurusan'=>$request->jurusan,
            'fakultas'=>$request->fakultas
        ]);
        return redirect('/tampilMahasiswa')->with('msg', 'Data mahasiswa berhasil disimpan!');
    }

    public function edit ($id){
        $mhs = DB::table('mahasiswa')->where('id',$id)->get();
        return view('mahasiswa.mahasiswaEdit',['data_mhs'=>$mhs]);
    }

    public function update(Request $request){
        DB::table('mahasiswa')->where('id',$request->id)->update([
            'nama'=>$request->nama,
            'nim'=>$request->nim,
            'email'=>$request->email,
            'no_telp'=>$request->no_telp,
            'prodi'=>$request->prodi,
            'jurusan'=>$request->jurusan,
            'fakultas'=>$request->fakultas
        ]);
        return redirect('/tampilMahasiswa')->with('msg', 'Data mahasiswa berhasil diedit!');
    }

    public function delete($id){
        DB::table('mahasiswa')->where('id',$id)->delete();
        return redirect('/tampilMahasiswa')->with('msg', 'Data mahasiswa berhasil dihapus!');
    }
}
