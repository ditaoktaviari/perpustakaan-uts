<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB; 

class DashboardController extends Controller
{
    public function index(){
        $mahasiswa = DB::table('mahasiswa')->count();
        $buku = DB::table('buku')
            ->select('stok_buku')
            ->sum('stok_buku');
        $transaksi_dipinjam = DB::table('transaksi')
            ->select('status_pinjam')
            ->where('status_pinjam', 0)
            ->count();
        $transaksi_kembali = DB::table('transaksi')
            ->select('status_pinjam')
            ->where('status_pinjam', 1)
            ->count();
        return view('dashboard.dashboard', [
            'page'=>'dashboard', 
            'mahasiswa'=>$mahasiswa, 
            'stok_buku'=>$buku, 
            'dipinjam'=>$transaksi_dipinjam, 
            'dikembalikan'=>$transaksi_kembali,
            ]);
    }
}
