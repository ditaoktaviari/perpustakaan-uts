<?php

namespace App\Http\Controllers\Buku;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB; 

class BukuController extends Controller
{
    public function tampilBuku(){
        $buku = DB::table('buku')->get();
        return view('buku.bukuList',['page'=>'buku', 'data_buku'=>$buku]);
    }

    public function create(){
        return view('buku.bukuCreate');
    }

    public function insert(Request $request){
        DB::table('buku')->insert([
            'judul_buku'=>$request->judul_buku,
            'pengarang'=>$request->pengarang,
            'penerbit'=>$request->penerbit,
            'tahun_terbit'=>$request->tahun_terbit,
            'tebal'=>$request->tebal,
            'isbn'=>$request->isbn,
            'stok_buku'=>$request->stok_buku,
            'biaya_sewa_harian'=>$request->biaya_sewa_harian
        ]);
        return redirect('/tampilBuku')->with('msg', 'Data buku berhasil disimpan!');
    }

    public function edit ($id){
        $buku = DB::table('buku')->where('id',$id)->get();
        return view('buku.bukuEdit',['data_buku'=>$buku]);
    }

    public function update(Request $request){
        DB::table('buku')->where('id',$request->id)->update([
            'judul_buku'=>$request->judul_buku,
            'pengarang'=>$request->pengarang,
            'penerbit'=>$request->penerbit,
            'tahun_terbit'=>$request->tahun_terbit,
            'tebal'=>$request->tebal,
            'isbn'=>$request->isbn,
            'stok_buku'=>$request->stok_buku,
            'biaya_sewa_harian'=>$request->biaya_sewa_harian
        ]);
        return redirect('/tampilBuku')->with('msg', 'Data buku berhasil diedit!');
    }

    public function delete($id){
        /* $status_pinjam = DB::table('transaksi')
            ->select('status_pinjam')
            ->get();
        $status_pinjam2 = DB::table('transaksi')
        ->join('buku', 'buku.id', '=', 'transaksi.id_buku')
        ->select('status_pinjam')
        ->where('status_pinjam', 1)
        ->get();

        if ($id == 'transaksi.id_buku' && $status_pinjam == 0) {
            return redirect('/tampilBuku');
        }else{ */
            DB::table('buku')->where('id',$id)->delete();
            return redirect('/tampilBuku')->with('msg', 'Data buku berhasil dihapus!');
        /* } */
    }
}
