<?php

namespace App\Http\Controllers\Transaksi;

use App\Http\Controllers\Controller;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB; 

class TransaksiController extends Controller
{
    public function tampilTransaksi(){
        $transaksi = DB::table('transaksi')
            ->join('mahasiswa', 'mahasiswa.id', '=', 'transaksi.id_mahasiswa')
            ->join('buku', 'buku.id', '=', 'transaksi.id_buku')
            ->select('mahasiswa.*', 'buku.*', 'transaksi.*')
            ->get();
        $status_pinjam = 'transaksi.status_pinjam';
        return view('transaksi.transaksiList',['page'=>'transaksi', 'data_transaksi'=>$transaksi, 'status_pinjam'=>$status_pinjam]);
    }

    public function create(){
        $mahasiswa = DB::table('mahasiswa')->get();
        $buku = DB::table('buku')->get();
        return view('transaksi.transaksiCreate', ['mahasiswa'=>$mahasiswa, 'buku'=>$buku]);
    }

    public function insert(Request $request){
        DB::table('transaksi')->insert([
            'id_mahasiswa'=>$request->id_mahasiswa,
            'id_buku'=>$request->id_buku,
            'tanggal_pinjam'=>$request->tanggal_pinjam,
            'status_pinjam'=>$request->status_pinjam,
            'total_biaya'=>$request->total_biaya,
        ]);
        
        DB::table('transaksi')->where('id', $request->id)->update([
            'stok_buku'=>$request->stok_buku - 1,
        ]);
        return redirect('/tampilTransaksi')->with('msg', 'Data transaksi berhasil disimpan!');
    }

    public function edit($id){
        $transaksi = DB::table('transaksi')->where('transaksi.id',$id)->get();
        $mahasiswa = DB::table('mahasiswa')->get();
        $buku = DB::table('buku')->get();
        return view('transaksi.transaksiEdit',['data_transaksi'=>$transaksi, 'mahasiswa'=>$mahasiswa, 'buku'=>$buku]);
    }

    public function update(Request $request){
/*         $status_pinjam = 'transaksi.status_pinjam';
        $biaya_harian = 'buku.biaya_sewa_harian';
        $tgl_pinjam = new DateTime('transaksi.tanggal_pinjam');
        $tgl_kembali = new DateTime('transaksi.tanggal_kembali');
        $selisih_tanggal = $tgl_pinjam->diff($tgl_kembali);
        $hari = $selisih_tanggal->format('%a');
        $total_biaya = $hari * $biaya_harian;
        
        if ($status_pinjam == 1) {
            DB::table('transaksi')->where('id',$request->id)->update([
                $total_biaya =>$request->total_biaya,
            ]);
        } else { */
            DB::table('transaksi')->where('id',$request->id)->update([
                'id_mahasiswa'=>$request->id_mahasiswa,
                'id_buku'=>$request->id_buku,
                'tanggal_pinjam'=>$request->tanggal_pinjam,
                'tanggal_kembali'=>$request->tanggal_kembali,
                'status_pinjam'=>$request->status_pinjam,
                'total_biaya'=>$request->total_biaya,
            ]);
       /*  } */
        
        return redirect('/tampilTransaksi')->with('msg', 'Data transaksi berhasil diedit!');
    }

    public function delete($id){
        DB::table('transaksi')->where('id',$id)->delete();
        return redirect('/tampilTransaksi')->with('msg', 'Data transaksi berhasil dihapus!');
    }
}
