@extends('layouts.main')

@section('title', 'Buku')

@section('page-title', 'Buku')

@section('nav-right')
    <li><a href="{{ route('tampilBuku') }}">Buku</a></li>
    <li class="active">Tambah buku</li>
@endsection

@section('konten')

    <div class="col-lg-12">
        <div class="card">
          <div class="card-header">
            <strong>Tambah</strong> Data Buku
          </div>
          <div class="card-body card-block">
            <form action="{{ route('insertBuku') }}" method="post" enctype="multipart/form-data" class="form-horizontal">
                {{ csrf_field() }} 
                <div class="row form-group">
                    <div class="col col-md-3"><label for="text-input" class=" form-control-label">Judul Buku</label></div>
                    <div class="col-12 col-md-9"><input type="text" id="text-input" name="judul_buku" required placeholder="Masukan judul buku..." class="form-control"></div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3"><label for="text-input" class=" form-control-label">Pengarang</label></div>
                    <div class="col-12 col-md-9"><input type="text" id="text-input" name="pengarang"  required placeholder="Masukan pengarang buku..." class="form-control"></div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3"><label for="text-input" class=" form-control-label">Penerbit</label></div>
                    <div class="col-12 col-md-9"><input type="text" id="text-input" name="penerbit" required placeholder="Masukan penerbit buku..." class="form-control"></div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3"><label for="text-input" class=" form-control-label">Tahun Terbit</label></div>
                    <div class="col-12 col-md-9"><input type="date" id="text-input" name="tahun_terbit" required placeholder="Masukan tahun terbit buku..." class="form-control"></div>
                </div>
                {{-- <div class="row form-group">
                    <div class="col col-md-3"><label for="select" class=" form-control-label">Tahun Terbit</label></div>
                    <div class="col-12 col-md-9">
                        <select id="select" name="tahun_terbit" class="form-control">
                        @php 
                            $now = date('Y');
                            for ($tahun = 1990; $tahun<=$now; $tahun++){
                                echo "<option value="$tahun">$tahun</option>";
                            }
                        @endphp
                        </select>
                    </div>
                </div> --}}
                <div class="row form-group">
                    <div class="col col-md-3"><label for="text-input" class=" form-control-label">Tebal</label></div>
                    <div class="col-12 col-md-9"><input type="text" id="text-input" name="tebal" required placeholder="Masukan tebal buku..." class="form-control"></div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3"><label for="text-input" class=" form-control-label">ISBN</label></div>
                    <div class="col-12 col-md-9"><input type="text" id="text-input" name="isbn" required placeholder="Masukan ISBN buku..." class="form-control"></div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3"><label for="text-input" class=" form-control-label">Stok Buku</label></div>
                    <div class="col-12 col-md-9"><input type="text" id="text-input" name="stok_buku" required placeholder="Masukan stok buku..." class="form-control"></div>
                </div>
                <div class="row form-group">
                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">Biaya Sewa Harian</label></div>
                        <div class="col-12 col-md-9"><input type="text" id="text-input" name="biaya_sewa_harian" required placeholder="Masukan biaya sewa harian buku..." class="form-control"></div>
                    </div>

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary btn-sm">Simpan</button>
                    <a href="{{ route('tampilMahasiswa') }}">
                        <button type="button" class="btn btn-danger btn-sm">Batal</button>
                    </a>
                </div>
            </form>
        </div>
    </div>
@endsection
