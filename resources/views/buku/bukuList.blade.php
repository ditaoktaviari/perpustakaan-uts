@extends('layouts.main')

@section('title', 'Buku')

@section('page-title', 'Buku')

@section('nav-right')
    <li><a href="{{ route('tampilBuku') }}">Buku</a></li>
@endsection

@section('konten')
<div class="content mt-3">
    @if (session('msg'))
        <div class="sufee-alert alert with-close alert-success alert-dismissible fade show">
            <span class="badge badge-pill badge-success">Success</span>
                {{ session('msg') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Data Buku</strong>
                        <div class="pull-right">
                            <a href="{{ route('createBuku') }}"><button type="button" class="btn btn-success">Tambah</button></a>
                        </div>
                    </div>
                    <div class="card-body">
                        <table id="bootstrap-data-table" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Judul Buku</th>
                                    <th>Pengarang</th>
                                    <th>Penerbit</th>
                                    <th>Tahun Terbit</th>
                                    <th>Tebal</th>
                                    <th>ISBN</th>
                                    <th>Stok Buku</th>
                                    <th>Biaya Sewa Harian</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data_buku as $data)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $data->judul_buku }}</td>
                                        <td>{{ $data->pengarang }}</td>
                                        <td>{{ $data->penerbit }}</td>
                                        <td>{{ $data->tahun_terbit }}</td>
                                        <td>{{ $data->tebal }}</td>
                                        <td>{{ $data->isbn }}</td>
                                        <td>{{ $data->stok_buku }}</td>
                                        <td>Rp {{ $data->biaya_sewa_harian }}</td>
                                        <td>
                                            <a href="{{ route('editBuku', [$data->id]) }}" class="btn btn-warning btn-sm" style="color:white">Edit</a>
                                            <a href="{{ route('deleteBuku', [$data->id]) }}" class="btn btn-danger btn-sm" onclick="return confirm('Anda yakin akan menghapus data?')">Hapus</button></a>
                                        </td>
                                    </tr>
                                @endforeach
                            <tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')
    <script src="assets/js/lib/data-table/datatables.min.js"></script>
    <script src="assets/js/lib/data-table/dataTables.bootstrap.min.js"></script>
    <script src="assets/js/lib/data-table/dataTables.buttons.min.js"></script>
    <script src="assets/js/lib/data-table/buttons.bootstrap.min.js"></script>
    <script src="assets/js/lib/data-table/jszip.min.js"></script>
    <script src="assets/js/lib/data-table/pdfmake.min.js"></script>
    <script src="assets/js/lib/data-table/vfs_fonts.js"></script>
    <script src="assets/js/lib/data-table/buttons.html5.min.js"></script>
    <script src="assets/js/lib/data-table/buttons.print.min.js"></script>
    <script src="assets/js/lib/data-table/buttons.colVis.min.js"></script>
    <script src="assets/js/lib/data-table/datatables-init.js"></script>


    <script type="text/javascript">
        $(document).ready(function() {
        $('#bootstrap-data-table-export').DataTable();
        } );
    </script>
@endsection

