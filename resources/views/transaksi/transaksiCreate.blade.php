@extends('layouts.main')

@section('title', 'Transaksi')

@section('page-title', 'Transaksi')

@section('nav-right')
    <li><a href="{{ route('tampilTransaksi') }}">Transaksi</a></li>
    <li class="active">Tambah transaksi</li>
@endsection

@section('konten')

    <div class="col-lg-12">
        <div class="card">
          <div class="card-header">
            <strong>Tambah</strong> Data Transaksi
          </div>
          <div class="card-body card-block">
            <form action="{{ route('insertTransaksi') }}" method="post" enctype="multipart/form-data" class="form-horizontal">

                {{ csrf_field() }} 

                <div class="row form-group">
                    <div class="col col-md-3"><label for="text-input" class=" form-control-label">Mahasiswa</label></div>
                    <div class="col-12 col-md-9">
                        <select name="id_mahasiswa" id="select" class="form-control">
                            @foreach ($mahasiswa as $row)
                                <option value="{{ $row->id }}">{{ $row->nim.' - '.$row->nama }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3"><label for="text-input" class=" form-control-label">Buku</label></div>
                    <div class="col-12 col-md-9">
                        <select name="id_buku" id="select" class="form-control">
                            @foreach ($buku as $row)
                                <option value="{{ $row->id }}">{{ $row->judul_buku.' - '.$row->pengarang }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3"><label for="text-input" class=" form-control-label">Tanggal Pinjam</label></div>
                    <div class="col-12 col-md-9"><input type="datetime-local" id="text-input" name="tanggal_pinjam" required placeholder="Masukan tanggal pinjam buku..." class="form-control"></div>
                </div>
                <input type="hidden" id="text-input" name="status_pinjam" value="0">
                <input type="hidden" id="text-input" name="total_biaya" value="0">

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary btn-sm">Simpan</button>
                    <a href="{{ route('tampilTransaksi') }}">
                        <button type="button" class="btn btn-danger btn-sm">Batal</button>
                    </a>
                </div>
            </form>
        </div>
    </div>
@endsection
