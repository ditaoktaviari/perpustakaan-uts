@extends('layouts.main')

@section('title', 'Transaksi')

@section('page-title', 'Transaksi')

@section('nav-right')
    <li><a href="{{ route('tampilTransaksi') }}">Transaksi</a></li>
    <li class="active">Edit transaksi</li>
@endsection

@section('konten')

    <div class="col-lg-12">
        <div class="card">
          <div class="card-header">
            <strong>Edit</strong> Data Transaksi
          </div>
          <div class="card-body card-block">
            @foreach ($data_transaksi as $data)
                <form action="{{ route('updateTransaksi') }}" method="post" enctype="multipart/form-data" class="form-horizontal">
                    {{ csrf_field() }} 
                    <input type="hidden" name="id" value="{{ $data->id }}">
                    <div class="row form-group">
                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">Mahasiswa</label></div>
                        <div class="col-12 col-md-9">
                            <select name="id_mahasiswa" id="select" class="form-control">
                                @foreach ($mahasiswa as $row)
                                    <option value="{{ $row->id }}" {{ $row->id == $data->id ? 'selected':'' }}>{{ $row->nim.' - '.$row->nama }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">Buku</label></div>
                        <div class="col-12 col-md-9">
                            <select name="id_buku" id="select" class="form-control">
                                @foreach ($buku as $row)
                                    <option value="{{ $row->id }}" {{ $row->id == $data->id ? 'selected':'' }}>{{ $row->judul_buku.' - '.$row->pengarang }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    {{-- <div class="row form-group">
                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">Tanggal Pinjam</label></div>
                        <div class="col-12 col-md-9"><input type="datetime-local" id="text-input" name="tanggal_pinjam" value="{{ $data->tanggal_pinjam }}" required placeholder="Masukan tanggal pinjam buku..." class="form-control"></div>
                    </div> --}}
                    <input type="hidden" name="tanggal_pinjam" value="{{ $data->tanggal_pinjam }}">
                    <div class="row form-group">
                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">Tanggal Kembali</label></div>
                        <div class="col-12 col-md-9"><input type="datetime-local" id="text-input" name="tanggal_kembali" value="{{ $data->tanggal_kembali }}" required placeholder="Masukan tanggal pengembalian buku..." class="form-control"></div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">Status Pinjam</label></div>
                        {{-- <div class="col-12 col-md-9"><input type="text" id="text-input" name="status_pinjam" value="{{ $data->status_pinjam }}" required placeholder="Masukan data buku..." class="form-control"></div> --}}
                        <div class="col-12 col-md-9">
                            <select name="status_pinjam" id="select" class="form-control">
                                <option value="0">dipinjam</option>
                                <option value="1">dikembalikan</option>
                            </select>
                        </div>
                    </div>
                    <input type="hidden" id="text-input" name="total_biaya" value="0">

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary btn-sm">Simpan</button>
                        <a href="{{ route('tampilTransaksi') }}">
                            <button type="button" class="btn btn-danger btn-sm">Batal</button>
                        </a>
                    </div>
                </form>
            @endforeach
        </div>
    </div>
@endsection
