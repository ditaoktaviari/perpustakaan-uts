@extends('layouts.main')

@section('title', 'Dashboard')

@section('page-title', 'Dashboard')

@section('nav-right', 'Dashboard')

@section('konten')

    <div class="content mt-3">
        <div class="col-lg-4 col-md-6">
            <div class="social-box facebook">
                <i class="fa fa-user"></i>
                <ul>
                    <li>
                        <strong>
                            <span>Mahasiswa</span><br>
                        </strong>
                        <br>
                    </li>
                    <li>
                        <strong>
                            <span class="count">
                                {{ $mahasiswa }}
                            </span>
                        </strong>
                    </li>
                </ul>
            </div>
            <!--/social-box-->
        </div>
        <div class="col-lg-4 col-md-6">
            <div class="social-box twitter">
                <i class="fa fa-book"></i>
                <ul>
                    <li>
                        <strong>
                            <span>Stok Buku</span><br>
                        </strong>
                        <br>
                    </li>
                    <li>
                        <strong>
                            <span class="count">
                                {{ $stok_buku }}
                            </span>
                        </strong>
                    </li>
                </ul>
            </div>
            <!--/social-box-->
        </div>
        <div class="col-lg-4 col-md-6">
            <div class="social-box google-plus">
                <i class="fa fa-bar-chart"></i>
                <ul>
                    <li>
                        <strong>
                            <span>Peminjaman</span><br>
                        </strong>
                        <span>Pengembalian</span>
                    </li>
                    <li>
                        <strong>
                            <span class="count">
                                {{ $dipinjam }}
                            </span>
                        </strong>
                        <span class="count">
                            {{ $dikembalikan }}
                        </span>
                    </li>
                </ul>
            </div>
            <!--/social-box-->
        </div>
    </div> 
@endsection

@section('js')
    <script src="assets/js/lib/chart-js/Chart.bundle.js"></script>
    <script src="assets/js/dashboard.js"></script>
    <script src="assets/js/widgets.js"></script>
    <script src="assets/js/lib/vector-map/jquery.vmap.js"></script>
    <script src="assets/js/lib/vector-map/jquery.vmap.min.js"></script>
    <script src="assets/js/lib/vector-map/jquery.vmap.sampledata.js"></script>
    <script src="assets/js/lib/vector-map/country/jquery.vmap.world.js"></script>
    <script>
        ( function ( $ ) {
            "use strict";

            jQuery( '#vmap' ).vectorMap( {
                map: 'world_en',
                backgroundColor: null,
                color: '#ffffff',
                hoverOpacity: 0.7,
                selectedColor: '#1de9b6',
                enableZoom: true,
                showTooltip: true,
                values: sample_data,
                scaleColors: [ '#1de9b6', '#03a9f5' ],
                normalizeFunction: 'polynomial'
            } );
        } )( jQuery );
    </script>
@endsection