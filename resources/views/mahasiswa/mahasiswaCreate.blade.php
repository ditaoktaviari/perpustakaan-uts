@extends('layouts.main')

@section('title', 'Mahasiswa')

@section('page-title', 'Mahasiswa')

@section('nav-right')
    <li><a href="{{ route('tampilMahasiswa') }}">Mahasiswa</a></li>
    <li class="active">Tambah mahasiswa</li>
@endsection

@section('konten')

    <div class="col-lg-12">
        <div class="card">
          <div class="card-header">
            <strong>Tambah</strong> Data Mahasiswa
          </div>
          <div class="card-body card-block">
            <form action="{{ route('insertMahasiswa') }}" method="post" enctype="multipart/form-data" class="form-horizontal">
                {{ csrf_field() }} 
                <div class="row form-group">
                    <div class="col col-md-3"><label for="text-input" class=" form-control-label">Nama</label></div>
                    <div class="col-12 col-md-9"><input type="text" id="text-input" name="nama" required placeholder="Masukan nama mahasiswa..." class="form-control"></div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3"><label for="text-input" class=" form-control-label">NIM</label></div>
                    <div class="col-12 col-md-9"><input type="text" id="text-input" name="nim" required placeholder="Masukan NIM mahasiswa..." class="form-control"></div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3"><label for="text-input" class=" form-control-label">Email</label></div>
                    <div class="col-12 col-md-9"><input type="email" id="text-input" name="email" required placeholder="Masukan email mahasiswa..." class="form-control"></div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3"><label for="text-input" class=" form-control-label">No Telp</label></div>
                    <div class="col-12 col-md-9"><input type="text" id="text-input" name="no_telp" required placeholder="Masukan no telp mahasiswa..." class="form-control"></div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3"><label for="text-input" class=" form-control-label">Prodi</label></div>
                    <div class="col-12 col-md-9"><input type="text" id="text-input" name="prodi" required placeholder="Masukan prodi mahasiswa..." class="form-control"></div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3"><label for="text-input" class=" form-control-label">Jurusan</label></div>
                    <div class="col-12 col-md-9"><input type="text" id="text-input" name="jurusan" required placeholder="Masukan jurusan mahasiswa..." class="form-control"></div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3"><label for="text-input" class=" form-control-label">Fakultas</label></div>
                    <div class="col-12 col-md-9"><input type="text" id="text-input" name="fakultas" required placeholder="Masukan fakultas mahasiswa..." class="form-control"></div>
                </div>

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary btn-sm">Simpan</button>
                    <a href="{{ route('tampilMahasiswa') }}">
                        <button type="button" class="btn btn-danger btn-sm">Batal</button>
                    </a>
                </div>
            </form>
        </div>
    </div>
@endsection
