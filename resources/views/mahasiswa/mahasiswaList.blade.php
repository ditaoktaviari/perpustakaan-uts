@extends('layouts.main')

@section('title', 'Mahasiswa')

@section('page-title', 'Mahasiswa')

@section('nav-right')
    <li><a href="{{ route('tampilMahasiswa') }}">Mahasiswa</a></li>
@endsection

@section('konten')
    
    <div class="content mt-3">
        @if (session('msg'))
            <div class="sufee-alert alert with-close alert-success alert-dismissible fade show">
                <span class="badge badge-pill badge-success">Success</span>
                    {{ session('msg') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Data Mahasiswa</strong>
                            <div class="pull-right">
                                <a href="{{ route('createMahasiswa') }}"><button type="button" class="btn btn-success">Tambah</button></a>
                            </div>
                        </div>
                        <div class="card-body">
                            <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>NIM</th>
                                        <th>Email</th>
                                        <th>No Telp</th>
                                        <th>Prodi</th>
                                        <th>Jurusan</th>
                                        <th>Fakultas</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($data_mhs as $data)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $data->nama }}</td>
                                            <td>{{ $data->nim }}</td>
                                            <td>{{ $data->email }}</td>
                                            <td>{{ $data->no_telp }}</td>
                                            <td>{{ $data->prodi }}</td>
                                            <td>{{ $data->jurusan }}</td>
                                            <td>{{ $data->fakultas }}</td>
                                            <td>
                                                <a href="{{ route('editMahasiswa', [$data->id]) }}"><button type="button" class="btn btn-warning btn-sm" style="color:white">Edit</button></a>
                                                <a href="{{ route('deleteMahasiswa', [$data->id]) }}"><button type="button" class="btn btn-danger btn-sm" onclick="return confirm('Anda yakin akan menghapus data?')">Hapus</button></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                <tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="assets/js/lib/data-table/datatables.min.js"></script>
    <script src="assets/js/lib/data-table/dataTables.bootstrap.min.js"></script>
    <script src="assets/js/lib/data-table/dataTables.buttons.min.js"></script>
    <script src="assets/js/lib/data-table/buttons.bootstrap.min.js"></script>
    <script src="assets/js/lib/data-table/jszip.min.js"></script>
    <script src="assets/js/lib/data-table/pdfmake.min.js"></script>
    <script src="assets/js/lib/data-table/vfs_fonts.js"></script>
    <script src="assets/js/lib/data-table/buttons.html5.min.js"></script>
    <script src="assets/js/lib/data-table/buttons.print.min.js"></script>
    <script src="assets/js/lib/data-table/buttons.colVis.min.js"></script>
    <script src="assets/js/lib/data-table/datatables-init.js"></script>


    <script type="text/javascript">
        $(document).ready(function() {
        $('#bootstrap-data-table-export').DataTable();
        } );
    </script>
@endsection

