@extends('layouts.main')

@section('title', 'Mahasiswa')

@section('page-title', 'Mahasiswa')

@section('nav-right')
    <li><a href="{{ route('tampilMahasiswa') }}">Mahasiswa</a></li>
    <li class="active">Edit mahasiswa</li>
@endsection

@section('konten')

    <div class="col-lg-12">
        <div class="card">
          <div class="card-header">
            <strong>Edit</strong> Data Mahasiswa
          </div>
          <div class="card-body card-block">
            @foreach ($data_mhs as $data)
                <form action="{{ route('updateMahasiswa') }}" method="post" enctype="multipart/form-data" class="form-horizontal">
                    {{ csrf_field() }} 
                    <input type="hidden" name="id" value="{{ $data->id }}">
                    <div class="row form-group">
                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">Nama</label></div>
                        <div class="col-12 col-md-9"><input type="text" id="text-input" name="nama" value="{{ $data->nama }}" required placeholder="Masukan nama anda..." class="form-control"></div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">NIM</label></div>
                        <div class="col-12 col-md-9"><input type="text" id="text-input" name="nim" value="{{ $data->nim }}" required placeholder="Masukan NIM anda..." class="form-control"></div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">Email</label></div>
                        <div class="col-12 col-md-9"><input type="email" id="text-input" name="email" value="{{ $data->email }}" required placeholder="Masukan email anda..." class="form-control"></div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">No Telp</label></div>
                        <div class="col-12 col-md-9"><input type="text" id="text-input" name="no_telp" value="{{ $data->no_telp }}" required placeholder="Masukan no telp anda..." class="form-control"></div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">Prodi</label></div>
                        <div class="col-12 col-md-9"><input type="text" id="text-input" name="prodi" value="{{ $data->prodi }}" required placeholder="Masukan prodi anda..." class="form-control"></div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">Jurusan</label></div>
                        <div class="col-12 col-md-9"><input type="text" id="text-input" name="jurusan" value="{{ $data->jurusan }}" required placeholder="Masukan jurusan anda..." class="form-control"></div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">Fakultas</label></div>
                        <div class="col-12 col-md-9"><input type="text" id="text-input" name="fakultas" value="{{ $data->fakultas }}" required placeholder="Masukan fakultas anda..." class="form-control"></div>
                    </div>

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary btn-sm">Simpan</button>
                        <a href="{{ route('tampilMahasiswa') }}">
                            <button type="button" class="btn btn-danger btn-sm">Batal</button>
                        </a>
                        
                    </div>
                </form>
            @endforeach
        </div>
    </div>
@endsection
