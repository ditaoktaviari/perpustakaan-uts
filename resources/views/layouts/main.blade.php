
<!doctype html>
<html class="no-js" lang="">
<head>
    @include('layouts.header')
</head>
<body>
    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">

            <div class="navbar-header">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="#">PERPUSTAKAAN</a>
            </div>
            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    @if ($page=='dashboard')
                        <li class="active">
                    @else
                        <li>
                    @endif
                        <a href="{{ route('dashboard') }}"> <i class="menu-icon fa fa-dashboard"></i>Dashboard </a>
                    </li>
                    @if ($page=='mhs')
                        <li class="active">
                    @else
                        <li>
                    @endif
                        <a href="{{ route('tampilMahasiswa') }}"> <i class="menu-icon fa fa-user"></i>Data Mahasiswa </a>
                    </li>
                    @if ($page=='buku')
                        <li class="active">
                    @else
                        <li>
                    @endif
                        <a href="{{ route('tampilBuku') }}"> <i class="menu-icon fa fa-book"></i>Data Buku </a>
                    </li>
                    @if ($page=='transaksi')
                        <li class="active">
                    @else
                        <li>
                    @endif
                        <a href="{{ route('tampilTransaksi') }}"> <i class="menu-icon fa fa-bar-chart"></i>Data Transaksi </a>
                    </li>
                </ul>
            </div>
        </nav>
    </aside>

    <div id="right-panel" class="right-panel">
        <!-- Header-->
        <header id="header" class="header">
            <div class="header-menu">
                <div class="col-sm-7">
                    <a id="menuToggle" class="menutoggle pull-left"><i class="fa fa fa-tasks"></i></a>
                </div>
            </div>
        </header><!-- /header -->
        <!-- Header-->
        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>
                            @yield('page-title')
                        </h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            @yield('nav-right')
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        @yield('konten')
    </div>

    @include('layouts.footer')

    @yield('js')

</body>
</html>
